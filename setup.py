"""Setup."""
from configparser import ConfigParser
from setuptools import setup
from pathlib import Path


# Configuración general
config_parser = ConfigParser()
with open(str(Path(__file__).parent / 'project.conf'), encoding='utf-8') as cstream:
    config_parser.read_file(cstream)
project_data = dict(config_parser.items('project-config'))
PROYECT_VERSION = project_data['version']
PROYECT_API_NAME = project_data['api_name']

setup(
    name='nodo-api',
    version=PROYECT_VERSION,
    author='Gabriel Andrés Aguirre',
    author_email='gabiandiagui@gmail.com',
    maintainer='Gabriel Andrés Aguirre',
    maintainer_email='gabiandiagui@gmail.com',
    description=PROYECT_API_NAME,
    classifiers=[
        'Framework :: Flask',
        'License :: OSI Approved :: GNU General Public License (GPL)',
        'Operating System :: OS Independent',
        'Programming Language :: Python :: 3.11',
        'Topic :: Internet'
    ],
    python_requires='>=3.11',
    license='GPL-3',
    include_package_data=True,
    package_data={
        '': [
            'project.conf',
        ],
    },
    install_requires=[
        'uwsgi >= 2.0.21',
        'Flask >= 2.2.4',
        'Flask-RESTful >= 0.3.9',
        'Flask-Cors >= 3.0.10',
        'psycopg2 >= 2.9.6',
        'flask-tryton >= 0.11.1',
        'marshmallow >= 3.19.0',
        'apispec >= 6.3.0',
        'apispec-webframeworks >= 0.5.2',
        'flask-swagger-ui >= 4.11.1',
        'trytond-nodo @ git+https://gitlab.com/gabiandi/desarrollos/nodo/nodo.git@develop',
    ],
)
