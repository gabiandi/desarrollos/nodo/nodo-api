"""Tests API doc"""
import os
import unittest
import configparser
from .conftest import flask_client


class TestApiDoc(unittest.TestCase):
    """Test de documentación de la API"""

    def setUp(self) -> None:
        super().setUp()

        # Configuración general
        project_data_parser = configparser.RawConfigParser()
        project_data_parser.read(os.path.dirname(__file__) + '/../project.conf')
        project_data = dict(project_data_parser.items('project-config'))
        self.proyect_swagger_ui_url = project_data['swagger_ui_url']
        self.proyect_swagger_url = project_data['swagger_url']

    def test_swagger(self):
        """Test de json de documentación"""
        response = flask_client.get(self.proyect_swagger_url)
        self.assertEqual(
            'application/json', response.headers['Content-Type'], 'Validación de tipo'
        )
        self.assertEqual(200, response.status_code, 'Validación de código de respuesta')

    def test_swagger_ui(self):
        """Test de página de documentación"""
        response = flask_client.get(self.proyect_swagger_ui_url)
        self.assertEqual(
            'text/html; charset=utf-8', response.headers['Content-Type'],
            'Validación de tipo'
        )
        self.assertEqual(308, response.status_code, 'Validación de código de respuesta')
