"""Test"""
from nodo_api import create_app
from nodo_api.config import get_config


flask_app = create_app()
flask_app.config.from_object(get_config('testing'))
flask_client = flask_app.test_client()
