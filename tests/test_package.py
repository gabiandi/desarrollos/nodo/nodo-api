"""Tests package"""
import unittest
import os
from flake8.api.legacy import get_style_guide


class TestPackage(unittest.TestCase):
    """Test del paquete"""

    def _get_python_filepaths(self):
        """Obtener los archivos de Python del proyecto."""
        python_paths = []
        for root in ['nodo_api', 'tests']:
            for dirpath, _, filenames in os.walk(root):
                for filename in filenames:
                    if filename.endswith('.py'):
                        python_paths.append(os.path.join(dirpath, filename))
        return python_paths

    def test_flake8(self):
        """Test de formato Flake8"""
        python_filepaths = self._get_python_filepaths()
        style_guide = get_style_guide()
        report = style_guide.check_files(python_filepaths)
        self.assertEqual(report.total_errors, 0, 'Reglas Flake8 del proyecto')
