"""Tests API base"""
import unittest
from .conftest import flask_client


class TestApiBase(unittest.TestCase):
    """Test base de la API"""

    def test_alive(self):
        """Test de alive contra recurso de prueba"""
        response = flask_client.get('/alive')
        self.assertEqual('"Alive!"\n', response.text, 'Validación de respuesta')
        self.assertEqual(200, response.status_code, 'Validación de código de respuesta')
