FROM python:3.11

USER root

COPY . /nodo/nodo-api
WORKDIR /nodo/nodo-api
RUN pip3 install -U . --break-system-packages

CMD ["uwsgi", \
    "--socket", "0.0.0.0:10000", \
    "--protocol=http", \
    "--module", "nodo_api.wsgi:app"]
