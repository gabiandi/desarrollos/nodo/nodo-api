"""
Esta API provee acceso a los recursos de **Nodo Brote Nativo**,
tales como productos, precios, etc.
"""
import secrets
from configparser import ConfigParser
from pathlib import Path
from flask import Flask, jsonify
from flask_cors import CORS
from flask_restful import Api
from flask_tryton import Tryton
from apispec import APISpec
from apispec.ext.marshmallow import MarshmallowPlugin
from apispec_webframeworks.flask import FlaskPlugin
from flask_swagger_ui import get_swaggerui_blueprint
from .config import config as api_config


# Configuración general
config_parser = ConfigParser()
with open(str(Path(__file__).parent.parent / 'project.conf'), encoding='utf-8') as cstream:
    config_parser.read_file(cstream)
project_data = dict(config_parser.items('project-config'))
PROYECT_VERSION = project_data['version']
PROYECT_API_NAME = project_data['api_name']
PROYECT_SWAGGER_UI_URL = project_data['swagger_ui_url']
PROYECT_SWAGGER_URL = project_data['swagger_url']

# Tryton
tryton_app = Tryton(configure_jinja=True)


def create_app() -> Flask:
    """Crear aplicación de Flask."""
    # Flask
    app = Flask(__name__)

    # Clave secreta para cookies de sesión
    app.secret_key = secrets.token_urlsafe(20)

    # Configuración
    app.config.from_object(api_config)

    # Api
    api = Api(app)

    # CORS
    CORS(app, resources={r'/*': {'origins': '*'}})

    # Tryton
    tryton_app.init_app(app)

    # APISpec
    spec = APISpec(
        title=PROYECT_API_NAME,
        version=PROYECT_VERSION,
        openapi_version='3.0.2',
        plugins=[FlaskPlugin(), MarshmallowPlugin()],
        info={
            'description': __doc__
        },
    )

    # Alive
    from .resources import alive
    res = alive.Alive
    path = '/alive'
    api.add_resource(res, path)

    method_view = res.as_view('Alive')
    app.add_url_rule(path, view_func=method_view)
    with app.test_request_context():
        spec.path(view=method_view)

    # Compañia
    from .resources import company
    res = company.Company
    path = '/company'
    api.add_resource(res, path)

    method_view = res.as_view('Company')
    app.add_url_rule(path, view_func=method_view)
    with app.test_request_context():
        spec.path(view=method_view)

    # Templates
    from .resources import products
    res = products.ProductTemplates
    path = '/product_templates'
    api.add_resource(res, path)

    method_view = res.as_view('ProductTemplates')
    app.add_url_rule(path, view_func=method_view)
    with app.test_request_context():
        spec.path(view=method_view)

    # Variants
    from .resources import products
    res = products.ProductVariants
    path = '/product_variants'
    api.add_resource(res, path)

    method_view = res.as_view('ProductVariants')
    app.add_url_rule(path, view_func=method_view)
    with app.test_request_context():
        spec.path(view=method_view)

    # Product categories
    from .resources import products
    res = products.ProductCategories
    path = '/product_categories'
    api.add_resource(res, path)

    method_view = res.as_view('ProductCategories')
    app.add_url_rule(path, view_func=method_view)
    with app.test_request_context():
        spec.path(view=method_view)

    # Products by location
    from .resources import products
    res = products.ProductByLocation
    path = '/product_by_location'
    api.add_resource(res, path)

    method_view = res.as_view('ProductsByLocation')
    app.add_url_rule(path, view_func=method_view)
    with app.test_request_context():
        spec.path(view=method_view)

    # Noticias
    from .resources import news
    res = news.News
    path = '/news'
    api.add_resource(res, path)

    method_view = res.as_view('News')
    app.add_url_rule(path, view_func=method_view)
    with app.test_request_context():
        spec.path(view=method_view)

    # Swagger UI
    swaggerui_blueprint = get_swaggerui_blueprint(
        PROYECT_SWAGGER_UI_URL,
        PROYECT_SWAGGER_URL,
        config={'app_name': PROYECT_API_NAME},
    )

    app.register_blueprint(swaggerui_blueprint)

    # Ruta del json generado por spec
    @app.route(PROYECT_SWAGGER_URL)
    def swagger():
        return jsonify(spec.to_dict())

    return app
