"""Schemas."""
from base64 import b64encode
from marshmallow import Schema, fields, pre_dump


class UomSchema(Schema):
    """Esquema de unidades de medida."""

    symbol = fields.String(metadata={'example': 'kg'})


class PartySchema(Schema):
    """Esquema de terceros."""

    name = fields.String(metadata={'example': 'Nodo Brote Nativo'})
    phone = fields.String(metadata={'example': '+54 9 345 401-4944'})
    email = fields.String(metadata={'example': 'nodo@gmail.com'})
    website = fields.String(metadata={'example': 'www.nodo.com.ar'})


class CurrencySchema(Schema):
    """Esquema de monedas."""

    code = fields.String(metadata={'example': 'ARS'})


class CompanySchema(Schema):
    """Esquema de compañia."""

    party = fields.Nested(PartySchema)
    currency = fields.Nested(CurrencySchema)


class ProductCategorySchema(Schema):
    """Esquema de categoria de producto."""

    name = fields.String(metadata={'example': 'Hortalizas'})


class ImageSchema(Schema):
    """Esquema de imágenes."""

    image = fields.String(metadata={'example': '/9j/4AAQSkZJRgABAQAAAQABAAD/'})

    def dump_schema(self, image):
        """Dump schema."""
        return {'image': b64encode(image.image).decode('utf-8')}

    @pre_dump(pass_many=True)
    def pre_dump_schema(self, images, many, **_):
        """Acción previa al dump."""
        return [self.dump_schema(image) for image in images] if many \
            else self.dump_schema(images)


class ProductVariantSchema(Schema):
    """Variante de producto."""

    code = fields.String(metadata={'example': 'VERD'})
    images = fields.Nested(ImageSchema, many=True)
    description = fields.String(metadata={'example': 'Manzana roja'})
    sale_uom = fields.Nested(UomSchema, metadata={'example': 'Kg'})
    list_price = fields.Float(metadata={'example': 600.50})
    categories = fields.Nested(ProductCategorySchema, many=True)


class ProductTemplateSchema(Schema):
    """Esquema de plantilla de producto."""

    name = fields.String(metadata={'example': 'Manzana'})
    code = fields.String(metadata={'example': 'MANZ'})
    categories = fields.Nested(ProductCategorySchema, many=True)
    products = fields.Nested(ProductVariantSchema, many=True)


class NewSchema(Schema):
    """Esquema de noticia."""

    title = fields.String(metadata={'example': 'Título'})
    url = fields.Url(metadata={'example': 'https://www.google.com'})
    image = fields.String(metadata={'example': '/9j/4AAQSkZJRgABAQAAAQABAAD/'})

    def dump_schema(self, new):
        """Dump schema."""
        return {
            'title': new.title,
            'url': new.url,
            'image': b64encode(new.image).decode('utf-8'),
        }

    @pre_dump(pass_many=True)
    def pre_dump_schema(self, news, many, **_):
        """Acción previa al dump."""
        return [self.dump_schema(new) for new in news] if many else self.dump_schema(news)


class ProductByLocationSchema(Schema):
    """Esquema de productos por ubicación."""

    product = fields.String(metadata={'example': 'MANZVERDE'})
    quantity = fields.Float(metadata={'example': 10.0})
