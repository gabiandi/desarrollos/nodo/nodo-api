"""Tryton."""
from . import tryton_app


User = tryton_app.pool.get('res.user')
Party = tryton_app.pool.get('party.party')
Company = tryton_app.pool.get('company.company')
ProductTemplate = tryton_app.pool.get('product.template')
ProductVariant = tryton_app.pool.get('product.product')
NodoNews = tryton_app.pool.get('nodo.news')
ProductCategory = tryton_app.pool.get('product.category')
StockLocation = tryton_app.pool.get('stock.location')


@tryton_app.default_context
def default_context():
    """Contexto por defecto en cada transacción de Tryton."""
    context = User.get_preferences(context_only=True)
    return context
