"""Metodos y atributos de utilidad."""
from functools import wraps
from flask import request


def get_query_parameters():
    """Decorador que parsea los query parameters y los filtros."""
    def outer_wrapper(func):
        @wraps(func)
        def inner_wrapper(*args, **kwargs):
            query_parameters = request.args.to_dict()
            query_filters = []
            request_filters = request.args.get('filters')

            if request_filters is not None:
                query_parameters.pop('filters')

                for request_filter in request_filters.split('|'):
                    (param, domain) = request_filter.split('::')
                    (operator, value) = domain.split(':')

                    if operator == 'eq':
                        operator = '='
                    elif operator == 'nq':
                        operator = '!='
                    elif operator == 'lt':
                        operator = '<'
                    elif operator == 'gt':
                        operator = '>'
                    elif operator == 'ltq':
                        operator = '<='
                    elif operator == 'gtq':
                        operator = '>='
                    elif operator == 'in':
                        value = value.split(',')
                    elif operator == 'notin':
                        operator = 'not in'
                        value = value.split(',')

                    if 'True' in value or 'False' in value:
                        if isinstance(value, str):
                            value = value == 'True'
                        elif isinstance(value, list):
                            value = [val == 'True' for val in value]

                    query_filters.append((param, operator, value))

            result = func(*args, query_parameters, query_filters, **kwargs)

            return result
        return inner_wrapper
    return outer_wrapper
