"""Configuración de Flask."""
import os


class Config(object):
    """Configuración base."""

    # Entorno
    DEBUG = bool(os.environ.get('DEBUG', '0') != '0')
    TESTING = bool(os.environ.get('TESTING', '0') != '0')

    # Configuración de tryton
    TRYTON_DATABASE = os.environ['TRYTON_DATABASE']


class ProductionConfig(Config):
    """Configuración de producción."""

    # Entorno
    DEBUG = bool(os.environ.get('DEBUG', '0') != '0')
    TESTING = bool(os.environ.get('TESTING', '0') != '0')
    NUM_PROXIES = int(os.environ.get('NUM_PROXIES', '1'))


class DevelopmentConfig(Config):
    """Configuración de desarrollo."""

    # Entorno
    DEBUG = bool(os.environ.get('DEBUG', '1') != '0')
    TESTING = bool(os.environ.get('TESTING', '0') != '0')
    NUM_PROXIES = int(os.environ.get('NUM_PROXIES', '0'))


class TestingConfig(Config):
    """Configuración de testing."""

    # Entorno
    DEBUG = bool(os.environ.get('DEBUG', '1') != '0')
    TESTING = bool(os.environ.get('TESTING', '1') != '0')
    NUM_PROXIES = int(os.environ.get('NUM_PROXIES', '1'))


def get_config(api_mode: str = None):
    """Obtener la configuración actual."""
    if api_mode == 'development':
        api_config = DevelopmentConfig()
    elif api_mode == 'testing':
        api_config = TestingConfig()
    else:
        api_config = ProductionConfig()
    return api_config


config = get_config(os.environ.get('API_MODE'))
