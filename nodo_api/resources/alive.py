"""Recurso de alive."""
from flask_restful import Resource


class Alive(Resource):
    'Alive'

    def get(self):
        """
        ---
        summary: Recurso para verificar la api.

        description: Recurso para verificar la api.

        responses:
          200:
            description: Estado de la api.
            required: true
            content:
              plain/text:
                schema:
                  type: string
                  example: Alive!

        tags:
          - Pruebas
        """
        return 'Alive!'
