"""Recurso para obtener noticias."""
from flask_restful import Resource
from .. import tryton_app
from ..utils import get_query_parameters
from ..schemas import NewSchema
from ..tryton import NodoNews


class News(Resource):
    'News'

    @get_query_parameters()
    @tryton_app.transaction()
    def get(self, _: dict, filters: list):
        """
        ---
        summary: Obtener noticias.

        description: Recurso para obtener noticias publicadas para mostrar.

        parameters:
          - in: query
            name: filters
            description: Filtros de busqueda.
            schema:
              type: string
              example: title::eq:Reunión

        responses:
          200:
            description: Noticias coincidentes con el filtro de busqueda.
            required: true
            content:
              application/json:
                schema:
                  type: array
                  items: NewSchema

        tags:
          - Noticias
        """
        return NewSchema().dump(NodoNews.search(filters), many=True)
