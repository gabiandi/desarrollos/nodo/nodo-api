"""Recurso para obtener variantes."""
from flask_restful import Resource
from trytond.transaction import Transaction
from .. import tryton_app
from ..utils import get_query_parameters
from ..schemas import (
    ProductVariantSchema, ProductCategorySchema, ProductByLocationSchema, ProductTemplateSchema)
from ..tryton import ProductVariant, ProductCategory, StockLocation, ProductTemplate


class ProductTemplates(Resource):
    'ProductTemplates'

    @get_query_parameters()
    @tryton_app.transaction()
    def get(self, _: dict, filters: list):
        """
        ---
        summary: Obtener plantillas de productos.

        description: Recurso para obtener plantillas de productos.

        parameters:
          - in: query
            name: filters
            description: Filtros de busqueda.
            schema:
              type: string
              example: categories::eq:Frutas

        responses:
          200:
            description: Lista de productos.
            required: true
            content:
              application/json:
                schema:
                  type: array
                  items: ProductTemplateSchema

        tags:
          - Productos
        """
        with Transaction().set_context(company=1):
            return ProductTemplateSchema().dump(ProductTemplate.search(filters), many=True)


class ProductVariants(Resource):
    'ProductVariants'

    @get_query_parameters()
    @tryton_app.transaction()
    def get(self, _: dict, filters: list):
        """
        ---
        summary: Obtener variantes de productos.

        description: Recurso para obtener variantes de productos.

        parameters:
          - in: query
            name: filters
            description: Filtros de busqueda.
            schema:
              type: string
              example: code::eq:MANZVERDE

        responses:
          200:
            description: Lista de productos.
            required: true
            content:
              application/json:
                schema:
                  type: array
                  items: ProductVariantSchema

        tags:
          - Productos
        """
        with Transaction().set_context(company=1):
            return ProductVariantSchema().dump(ProductVariant.search(filters), many=True)


class ProductCategories(Resource):
    'ProductCategories'

    @tryton_app.transaction()
    def get(self):
        """
        ---
        summary: Obtener categorias de productos del catálogo.

        description: Recurso para obtener categorias productos del catálogo.

        responses:
          200:
            description: Lista de categorias de productos.
            required: true
            content:
              application/json:
                schema:
                  type: array
                  items: ProductCategorySchema

        tags:
          - Productos
        """
        return ProductCategorySchema().dump(ProductCategory.search([]), many=True)


class ProductByLocation(Resource):
    'ProductByLocation'

    @get_query_parameters()
    @tryton_app.transaction()
    def get(self, parameters: dict, _: list):
        """
        ---
        summary: Obtener productos por ubicación.

        description: Recurso para obtener productos por ubicación.

        parameters:
          - in: query
            name: warehouse
            description: Código de ubicación de busqueda.
            schema:
              type: string
              example: STO
          - in: query
            name: product
            description: Producto de busqueda.
            schema:
              type: string
              example: MANZVERDE

        responses:
          200:
            description: Lista de productos en dicha ubicación.
            required: true
            content:
              application/json:
                schema:
                  type: array
                  items: ProductByLocationSchema

        tags:
          - Productos
        """
        # Comprobación de parametros
        if not parameters.get('warehouse'):
            return 'Debe de especificar el warehouse.', 400
        elif not parameters.get('product'):
            return 'Debe de especificar el producto para buscar.', 400

        # Se busca la location
        try:
            warehouse, = StockLocation.search([('code', '=', parameters['warehouse'])])
        except ValueError:
            return f'No se encuentra la ubicación {parameters["warehouse"]}', 404

        # Se buscan los productos en esa location
        products_by_warehouse = ProductVariant.products_by_location(
            [warehouse.id], grouping=('product',)
        )

        # Se recorren los resultados
        result = {}

        for key in products_by_warehouse.keys():
            product = ProductVariant(key[1])

            if product.code == parameters['product']:
                result = {
                    'product': product.code,
                    'quantity': products_by_warehouse[key],
                }
                break

        return ProductByLocationSchema().dump(result)
