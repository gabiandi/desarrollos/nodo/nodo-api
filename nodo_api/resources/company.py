"""Recurso para obtener información de compañia."""
from flask_restful import Resource
from .. import tryton_app
from ..utils import get_query_parameters
from ..schemas import CompanySchema
from ..tryton import Company as tCompany


class Company(Resource):
    'Company'

    @get_query_parameters()
    @tryton_app.transaction()
    def get(self, _: dict, filters: list):
        """
        ---
        summary: Obtener información de empresas.

        description: Recurso para obtener información de empresas.

        parameters:
          - in: query
            name: filters
            description: Filtros de busqueda.
            schema:
              type: string
              example: id::eq:1

        responses:
          200:
            description: Lista de empresas.
            required: true
            content:
              application/json:
                schema:
                  type: array
                  items: CompanySchema

        tags:
          - Compañia
        """
        return CompanySchema().dump(tCompany.search(filters), many=True)
