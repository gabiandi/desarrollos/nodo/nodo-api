"""Módulo para hacer loggeos."""
import os
import logging


# Variables de entorno
DEBUG = bool(os.environ.get('DEBUG', '0') != '0')
LOG_FORMAT = os.environ.get(
    'LOG_FORMAT',
    '[%(asctime)s] - [%(filename)s %(lineno)d] %(levelname)s:'
)
LOG_DATE_FORMAT = os.environ.get('LOG_DATE_FORMAT', '%Y:%m:%d %H:%M:%S')
LOG_COLOR = bool(os.environ.get('LOG_COLOR', '0') != '0')


class LogFormatter(logging.Formatter):
    """Formateador del log."""

    grey = '\x1b[38;20m'
    blue = '\x1b[34;20m'
    yellow = '\x1b[33;20m'
    red = '\x1b[31;20m'
    bold_red = '\x1b[31;1m'
    reset = '\x1b[0m'

    FORMATS = {
        logging.DEBUG: LOG_FORMAT + ' ' + grey + '%(message)s' + reset,
        logging.INFO: LOG_FORMAT + ' ' + blue + '%(message)s' + reset,
        logging.WARNING: LOG_FORMAT + ' ' + yellow + '%(message)s' + reset,
        logging.ERROR: LOG_FORMAT + ' ' + red + '%(message)s' + reset,
        logging.CRITICAL: LOG_FORMAT + ' ' + bold_red + '%(message)s' + reset,
    }

    def format(self, record):
        log_formatter = logging.Formatter(
            fmt=self.FORMATS.get(record.levelno),
            datefmt=LOG_DATE_FORMAT
        ) if LOG_COLOR \
            else logging.Formatter(
            fmt=LOG_FORMAT + ' ' + '%(message)s',
            datefmt=LOG_DATE_FORMAT
        )
        return log_formatter.format(record)


def create_custom_logger():
    """Crear logger."""
    formatter = LogFormatter()
    ch = logging.StreamHandler()
    ch.setFormatter(formatter)
    custom_logger = logging.getLogger(__name__)
    custom_logger.addHandler(ch)
    custom_logger.setLevel(logging.DEBUG if DEBUG else logging.INFO)
    custom_logger.propagate = False
    return custom_logger


log = create_custom_logger()
